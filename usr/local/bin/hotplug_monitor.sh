#! /usr/bin/bash

export DISPLAY=:0
export XAUTHORITY=/home/marcotti/.Xauthority

function connect(){
    xrandr --output HDMI-A-0 --auto --primary --mode 1920x1080 --pos 0x0 --rotate normal --output eDP --mode 1920x1080 --pos 1920x290 --rotate normal --output DisplayPort-0 --off --output DisplayPort-1 --off
    i3-msg '[workspace="2"]' move workspace to output primary
}

function disconnect(){
    xrandr --output HDMI-A-0 --off
}

xrandr | grep "HDMI-A-0 connected" &> /dev/null && connect || disconnect
