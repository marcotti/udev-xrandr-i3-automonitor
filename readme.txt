udev-xrandr-i3-automonitor
==========================

udev rules to automatically execute some xrandr
config when a monitor is plugged. Also moves the
i3 "2" workspace to the plugged monitor.

things to look out for
----------------------

- run "udevadm monitor --environment --udev" and
see your DEVNAME to change it on KERNEL part of
the udev rule.

- of course everywhere it says "YOUR_USER" should
be changed for your username.

- run "xrandr" with the output connected and see
what's the name. You need to change "HDMI-A-0"
for your output's name.


